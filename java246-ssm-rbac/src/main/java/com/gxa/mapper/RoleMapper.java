package com.gxa.mapper;

import com.gxa.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer roleId);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleId);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    /**
     * 通过用户id查询角色
     * @param userId
     * @return
     */
    List<Role> findByUserId(@Param("userId") Integer userId);
    /**
     * 通过用户id查询角色
     * @param userId
     * @return
     */
    List<Role> findByUserId2(@Param("userId") Integer userId);
}