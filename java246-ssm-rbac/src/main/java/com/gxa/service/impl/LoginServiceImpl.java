package com.gxa.service.impl;

import com.gxa.dto.ResultDto;
import com.gxa.exception.SystemException;
import com.gxa.group.LoginPhone;
import com.gxa.mapper.UserMapper;
import com.gxa.pojo.User;
import com.gxa.service.LoginService;
import com.gxa.util.Response;
import com.gxa.validator.MyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service("phoneLoginService")
public class LoginServiceImpl implements LoginService {

    /**
     * 校验器
     */
    @Autowired
    private MyValidator validator;

    @Autowired
    private UserMapper userMapper;


    /**
     * 手机号登录
     * @param user
     * @param session
     * @return
     */
    @Override
    public ResultDto login(User user, HttpSession session) {
        // 数据校验
        validator.validate(user, LoginPhone.class);
        // 查询数据库
        User dbUser = userMapper.findByPhone(Long.valueOf(user.getUserPhone()));
        if (dbUser == null) {
            throw new SystemException(1001, "该手机号不存在！");
        }
        // 保存用户名
        session.setAttribute("userName",dbUser.getUserName());
        // 清空code
        session.removeAttribute("vcode");
        return Response.success("登录成功！");
    }
}
