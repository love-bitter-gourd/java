package com.gxa.service.impl;

import com.gxa.dto.ResultDto;
import com.gxa.exception.SystemException;
import com.gxa.group.LoginName;
import com.gxa.mapper.AuthMapper;
import com.gxa.mapper.UserMapper;
import com.gxa.pojo.Auth;
import com.gxa.pojo.Menu;
import com.gxa.pojo.Role;
import com.gxa.pojo.User;
import com.gxa.service.LoginService;
import com.gxa.util.MD5Util;
import com.gxa.util.Response;
import com.gxa.validator.MyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service("nameLoginService")
public class LoginServiceByNameImpl implements LoginService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AuthMapper authMapper;
    @Autowired
    private MyValidator validator;

    @Override
    public ResultDto login(User user, HttpSession session) {
        // 校验
        validator.validate(user, LoginName.class);
        // 查询数据库
        User dbUser = userMapper.findByName(user.getUserName());
        if (dbUser == null) {
            throw new SystemException(1003, "该用户不存在！");
        }
        // 判断密码
        if (!dbUser.getUserPassword().equals(MD5Util.MD55(user.getUserPassword()))) {
            throw new SystemException(1004, "密码错误！");
        }
        // 登录成功
        session.setAttribute("userName", dbUser.getUserName());
        // 判断是否是超级管理员
        Boolean isSuper = isSuper(dbUser);
        session.setAttribute("isSuper", isSuper);
        if (isSuper){
            // 将所有的菜单数据全部获取到
            List<Auth> allMenu = authMapper.findAllMenu();
            List<Menu> menus = getMenu(allMenu);
            session.setAttribute("menus", menus);
        }else{
            // 处理菜单数据
            List<Auth> authList = getAuth(dbUser);
            List<Menu> menus = getMenu(authList);
            session.setAttribute("menus", menus);
            // 处理权限数据
            session.setAttribute("authList", authList);
        }
        return Response.success("登录成功！");
    }
    /**
     * 判断是否是超级管理员
     * @param user
     * @return
     */
    private Boolean isSuper(User user){
        List<Role> roles = user.getRoles();
        for (Role role : roles) {
            System.out.println(role);
            if (role == null || role.getIsSuper() == null){
                continue;
            }
            if (role.getIsSuper().equals(1)){
                return true;
            }
        }
        return false;
    }
    /**
     * 从用户中去获取权限列表
     * @param user
     * @return
     */
    private List<Auth> getAuth(User user){
        // 权限容器
        List<Auth> authList = new ArrayList<>();
        List<Role> roles = user.getRoles();

        for (Role role : roles) {
            authList.addAll(role.getAuthList());
        }
        return authList;
    }
    /**
     * 获取菜单结构
     * @param authList
     * @return
     */
    private List<Menu> getMenu(List<Auth> authList){
        // 先创建菜单的容器
        List<Menu> menus = new ArrayList<>();
        // 先处理一级菜单
        for (Auth auth : authList) {
            // 一级菜单
            if (auth.getIsMenu().equals(1) && auth.getAuthLevel().equals(1)){
                menus.add(new Menu(auth.getAuthName(), auth.getAuthUrl(), auth.getAuthLevel(), auth.getAuthId(),null));
            }
        }
        // 二级菜单
        for (Menu menu : menus) {
            // 子菜单集合
            List<Menu> subMenus = new ArrayList<>();
            for (Auth auth : authList) {
                if (auth.getIsMenu().equals(1) && auth.getAuthLevel().equals(2)){
                    if (menu.getMenuId().equals(auth.getAuthParentId())){
                        subMenus.add(new Menu(auth.getAuthName(), auth.getAuthUrl(), auth.getAuthLevel(), auth.getAuthId(),null));
                    }
                }
            }
            menu.setSubMenu(subMenus);
        }

        return menus;
    }
}
