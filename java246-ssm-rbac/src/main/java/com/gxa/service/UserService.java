package com.gxa.service;

import com.gxa.dto.MyParam;
import com.gxa.dto.ResultDto;

public interface UserService {


    /**
     * 管理员列表
     * @param param
     * @return
     */
    ResultDto list(MyParam param);


    /**
     * 删除数据
     * @param userId
     * @return
     */
    ResultDto delete(Integer userId);


}
