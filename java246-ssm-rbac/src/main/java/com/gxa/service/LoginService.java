package com.gxa.service;

import com.gxa.dto.ResultDto;
import com.gxa.pojo.User;

import javax.servlet.http.HttpSession;

public interface LoginService {


    /**
     * 用户登录
     * @param user
     * @return
     */
    ResultDto login(User user, HttpSession session);


}
