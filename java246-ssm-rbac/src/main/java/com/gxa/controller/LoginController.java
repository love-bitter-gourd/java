package com.gxa.controller;

import com.gxa.dto.ResultDto;
import com.gxa.pojo.User;
import com.gxa.service.LoginService;
import com.gxa.util.MD5Util;
import com.gxa.util.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    /**
     * 注入手机号登录
     */
    @Resource(name = "phoneLoginService")
    private LoginService loginService;
    @Resource(name = "nameLoginService")
    private LoginService loginService2;

    @GetMapping("/login/page")
    public String loginPage(){
        return "login";
    }

    @GetMapping("/login/phone/page")
    public String loginByPhonePage(){
        return "loginPhone";
    }

    /**
     * 登录操作
     * @param user
     * @return
     */
    @PostMapping("/login/do")
    @ResponseBody
    public ResultDto login(User user, HttpSession session){
        return loginService.login(user, session);
    }

    /**
     * 登录操作-通过用户名
     * @param user
     * @return
     */
    @PostMapping("/login/do/name")
    @ResponseBody
    public ResultDto loginName(User user, HttpSession session){
        return loginService2.login(user, session);
    }


    /**
     * 登出操作
     * @return
     */
    @PostMapping("/login/out")
    @ResponseBody
    public ResultDto out(HttpSession session){
        session.removeAttribute("userName");
        return Response.success();
    }

}
