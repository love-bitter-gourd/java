package com.gxa.exception;

import com.gxa.dto.ResultDto;
import com.gxa.util.Response;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class MyExceptionHandler {


    /**
     * 捕获系统异常
     * @param e
     * @return
     */
    @ExceptionHandler(SystemException.class)
    @ResponseBody
    public ResultDto systemExceptionHandler(SystemException e){
        // 记录日志
        return Response.error(e.getCode(), e.getMsg());
    }

    /**
     * 捕获系统异常-非ajax请求
     * @param e
     * @return
     */
    @ExceptionHandler(NoAjaxException.class)
    public ModelAndView noAjaxExceptionHandler(NoAjaxException e){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        return modelAndView;
    }


    /**
     * 捕获所有异常
     * @param e
     * @return
     */
    //@ExceptionHandler(Exception.class)
    public ResultDto exceptionHandler(Exception e){
        // 记录日志
        return Response.error(1001, "未知异常！");
    }


}
