package com.gxa.pojo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gxa.group.Add;
import com.gxa.group.LoginName;
import com.gxa.group.LoginPhone;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

/**
 * user
 * @author 
 */
@Data
public class User implements Serializable {
    private Integer userId;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空！", groups = {LoginName.class, Add.class})
    private String userName;

    /**
     * 用户手机号
     */
    @Pattern(regexp = "^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$",message = "手机号格式不正确！", groups = {LoginPhone.class, Add.class})
    private String userPhone;

    /**
     * 用户密码
     */
    @NotBlank(message = "密码不能为空！", groups = {LoginName.class, Add.class})
    private String userPassword;

    /**
     * 用户创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    /**
     * 最后登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private Timestamp lastLoginTime;

    /**
     * 用户所有用的角色
     */
    private List<Role> roles;

    private static final long serialVersionUID = 1L;
}