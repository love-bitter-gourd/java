<%@page contentType="text/html; charset=utf-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>后台登录</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/font.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/xadmin.js"></script>

</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up">
        <div class="message">管理登录</div>
        <div id="darkbannerwrap"></div>
        
        <form method="post" class="layui-form" >
            <input name="userPhone" placeholder="请输入手机号"  type="text" lay-verify="required" class="layui-input" >
            <hr class="hr15">
            <div class="layui-row">
                <div class="layui-col-md6">
                    <input name="vcode" lay-verify="required" placeholder="验证码"  type="text" class="layui-input">
                </div>
                <div class="layui-col-md6">
                    <button type="button" id="sendMsg" class="layui-btn layui-btn-lg layui-btn-normal">获取验证码</button>
                </div>
            </div>
            <hr class="hr15">
            <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
            <hr class="hr20" >
        </form>
        <a href="${pageContext.request.contextPath}/login/page">用户名密码登录</a>
    </div>

    <script>
        $(function  () {
            layui.use('form', function(){
              var form = layui.form;
              //监听提交
              form.on('submit(login)', function(data){
                  // 获取数据
                  var postData = data.field;
                  // 校验手机号
                  if (!/^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/.test(postData.userPhone)){
                      layer.msg("手机号不正确！")
                      return false;
                  }
                  // 校验验证码
                  console.log(postData.vcode);
                  if (!checkVcode(postData.vcode)){
                      layer.msg("验证码错误！")
                      return false;
                  }
                  // 提交数据登录
                  $.ajax({
                      type:"post",
                      url:"${pageContext.request.contextPath}/login/do",
                      data:postData,
                      dataType:"json",
                      success:function (data) {
                          layer.msg(data.msg,function(){
                              if (data.code === 200){
                                  location.href="${pageContext.request.contextPath}/index";
                              }
                          });

                      }
                  });
                return false;
              });
            });

            // 发送验证码
            $("#sendMsg").click(function () {
                // 获取手机号
                var phone = $("input[name=userPhone]").val();
                if (!/^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/.test(phone)){
                    layer.msg("手机号不正确！")
                    return;
                }

                $.ajax({
                    type:"post",
                    url:"${pageContext.request.contextPath}/sms/send",
                    data:{phone: phone},
                    dataType:"json",
                    success:function (data) {
                        layer.msg(data.msg);
                        if (data.code === 200){
                            // 禁用发送按钮
                            $("#sendMsg").prop("disabled", true);
                            $("#sendMsg").removeClass("layui-btn-normal");
                            $("#sendMsg").addClass("layui-btn-disabled");
                            // 启动定时器
                            var sends = 60;
                            var timer = setInterval(function () {
                                $("#sendMsg").text("发送成功("+sends+"S)");
                                sends--;
                                if (sends < 0){
                                    clearInterval(timer);
                                    // 启用按钮
                                    $("#sendMsg").text("获取验证码");
                                    $("#sendMsg").prop("disabled", false);
                                    $("#sendMsg").removeClass("layui-btn-disabled");
                                    $("#sendMsg").addClass("layui-btn-normal");
                                }
                            }, 1000);
                        }
                    }
                });



            })
        });


        /**
         * 校验验证码
         * @param vcode
         */
        function checkVcode(vcode) {
            var res = false;
            $.ajax({
                type:"post",
                url:"${pageContext.request.contextPath}/sms/check",
                data:{vcode: vcode},
                dataType:"json",
                async: false,
                success:function (data) {
                    if (data.code === 200){
                        res = true;
                    }
                }
            });
            return res;
        }

        
    </script>

    
    <!-- 底部结束 -->

</body>
</html>