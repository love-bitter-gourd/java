import com.gxa.mapper.AuthMapper;
import com.gxa.mapper.UserMapper;
import com.gxa.pojo.Auth;
import com.gxa.pojo.Menu;
import com.gxa.pojo.Role;
import com.gxa.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config-dao.xml"})
public class SpringTest {

    @Autowired
    private UserMapper mapper;
    @Autowired
    private AuthMapper authMapper;

    @Test
    public void test01(){
        User admin = mapper.findByName("rose");
        System.out.println(admin);
        for (Role role : admin.getRoles()) {
            System.out.println(role);
            for (Auth auth : role.getAuthList()) {
                System.out.println(auth);
            }
        }
    }


    @Test
    public void test02(){
        List<Auth> allMenu = authMapper.findAllMenu();
        List<Menu> menus = getMenu(allMenu);
        for (Menu menu : menus) {
            System.out.println(menu);
        }

    }

    private List<Menu> getMenu(List<Auth> authList){
        // 先创建菜单的容器
        List<Menu> menus = new ArrayList<>();
        // 先处理一级菜单
        for (Auth auth : authList) {
            // 一级菜单
            if (auth.getIsMenu().equals(1) && auth.getAuthLevel().equals(1)){
                menus.add(new Menu(auth.getAuthName(), auth.getAuthUrl(), auth.getAuthLevel(), auth.getAuthId(),null));
            }
        }
        // 二级菜单
        for (Menu menu : menus) {
            // 子菜单集合
            List<Menu> subMenus = new ArrayList<>();
            for (Auth auth : authList) {
                if (auth.getIsMenu().equals(1) && auth.getAuthLevel().equals(2)){
                    if (menu.getMenuId().equals(auth.getAuthParentId())){
                        subMenus.add(new Menu(auth.getAuthName(), auth.getAuthUrl(), auth.getAuthLevel(), auth.getAuthId(),null));
                    }
                }
            }
            menu.setSubMenu(subMenus);
        }

        return menus;
    }
}
