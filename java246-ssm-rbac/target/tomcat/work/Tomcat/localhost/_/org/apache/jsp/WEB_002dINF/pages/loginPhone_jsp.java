/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.47
 * Generated at: 2021-03-16 07:17:55 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.pages;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class loginPhone_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("<!doctype html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("\t<meta charset=\"UTF-8\">\n");
      out.write("\t<title>后台登录</title>\n");
      out.write("\t<meta name=\"renderer\" content=\"webkit|ie-comp|ie-stand\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi\" />\n");
      out.write("    <meta http-equiv=\"Cache-Control\" content=\"no-siteapp\" />\n");
      out.write("\n");
      out.write("    <link rel=\"shortcut icon\" href=\"/favicon.ico\" type=\"image/x-icon\" />\n");
      out.write("    <link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/static/css/font.css\">\n");
      out.write("\t<link rel=\"stylesheet\" href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/static/css/xadmin.css\">\n");
      out.write("    <script type=\"text/javascript\" src=\"https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js\"></script>\n");
      out.write("    <script src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/static/lib/layui/layui.js\" charset=\"utf-8\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/static/js/xadmin.js\"></script>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<body class=\"login-bg\">\n");
      out.write("    \n");
      out.write("    <div class=\"login layui-anim layui-anim-up\">\n");
      out.write("        <div class=\"message\">管理登录</div>\n");
      out.write("        <div id=\"darkbannerwrap\"></div>\n");
      out.write("        \n");
      out.write("        <form method=\"post\" class=\"layui-form\" >\n");
      out.write("            <input name=\"userPhone\" placeholder=\"请输入手机号\"  type=\"text\" lay-verify=\"required\" class=\"layui-input\" >\n");
      out.write("            <hr class=\"hr15\">\n");
      out.write("            <div class=\"layui-row\">\n");
      out.write("                <div class=\"layui-col-md6\">\n");
      out.write("                    <input name=\"vcode\" lay-verify=\"required\" placeholder=\"验证码\"  type=\"text\" class=\"layui-input\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"layui-col-md6\">\n");
      out.write("                    <button type=\"button\" id=\"sendMsg\" class=\"layui-btn layui-btn-lg layui-btn-normal\">获取验证码</button>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <hr class=\"hr15\">\n");
      out.write("            <input value=\"登录\" lay-submit lay-filter=\"login\" style=\"width:100%;\" type=\"submit\">\n");
      out.write("            <hr class=\"hr20\" >\n");
      out.write("        </form>\n");
      out.write("        <a href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/login/page\">用户名密码登录</a>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <script>\n");
      out.write("        $(function  () {\n");
      out.write("            layui.use('form', function(){\n");
      out.write("              var form = layui.form;\n");
      out.write("              //监听提交\n");
      out.write("              form.on('submit(login)', function(data){\n");
      out.write("                  // 获取数据\n");
      out.write("                  var postData = data.field;\n");
      out.write("                  // 校验手机号\n");
      out.write("                  if (!/^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/.test(postData.userPhone)){\n");
      out.write("                      layer.msg(\"手机号不正确！\")\n");
      out.write("                      return false;\n");
      out.write("                  }\n");
      out.write("                  // 校验验证码\n");
      out.write("                  console.log(postData.vcode);\n");
      out.write("                  if (!checkVcode(postData.vcode)){\n");
      out.write("                      layer.msg(\"验证码错误！\")\n");
      out.write("                      return false;\n");
      out.write("                  }\n");
      out.write("                  // 提交数据登录\n");
      out.write("                  $.ajax({\n");
      out.write("                      type:\"post\",\n");
      out.write("                      url:\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/login/do\",\n");
      out.write("                      data:postData,\n");
      out.write("                      dataType:\"json\",\n");
      out.write("                      success:function (data) {\n");
      out.write("                          layer.msg(data.msg,function(){\n");
      out.write("                              if (data.code === 200){\n");
      out.write("                                  location.href=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/index\";\n");
      out.write("                              }\n");
      out.write("                          });\n");
      out.write("\n");
      out.write("                      }\n");
      out.write("                  });\n");
      out.write("                return false;\n");
      out.write("              });\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            // 发送验证码\n");
      out.write("            $(\"#sendMsg\").click(function () {\n");
      out.write("                // 获取手机号\n");
      out.write("                var phone = $(\"input[name=userPhone]\").val();\n");
      out.write("                if (!/^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/.test(phone)){\n");
      out.write("                    layer.msg(\"手机号不正确！\")\n");
      out.write("                    return;\n");
      out.write("                }\n");
      out.write("\n");
      out.write("                $.ajax({\n");
      out.write("                    type:\"post\",\n");
      out.write("                    url:\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/sms/send\",\n");
      out.write("                    data:{phone: phone},\n");
      out.write("                    dataType:\"json\",\n");
      out.write("                    success:function (data) {\n");
      out.write("                        layer.msg(data.msg);\n");
      out.write("                        if (data.code === 200){\n");
      out.write("                            // 禁用发送按钮\n");
      out.write("                            $(\"#sendMsg\").prop(\"disabled\", true);\n");
      out.write("                            $(\"#sendMsg\").removeClass(\"layui-btn-normal\");\n");
      out.write("                            $(\"#sendMsg\").addClass(\"layui-btn-disabled\");\n");
      out.write("                            // 启动定时器\n");
      out.write("                            var sends = 60;\n");
      out.write("                            var timer = setInterval(function () {\n");
      out.write("                                $(\"#sendMsg\").text(\"发送成功(\"+sends+\"S)\");\n");
      out.write("                                sends--;\n");
      out.write("                                if (sends < 0){\n");
      out.write("                                    clearInterval(timer);\n");
      out.write("                                    // 启用按钮\n");
      out.write("                                    $(\"#sendMsg\").text(\"获取验证码\");\n");
      out.write("                                    $(\"#sendMsg\").prop(\"disabled\", false);\n");
      out.write("                                    $(\"#sendMsg\").removeClass(\"layui-btn-disabled\");\n");
      out.write("                                    $(\"#sendMsg\").addClass(\"layui-btn-normal\");\n");
      out.write("                                }\n");
      out.write("                            }, 1000);\n");
      out.write("                        }\n");
      out.write("                    }\n");
      out.write("                });\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            })\n");
      out.write("        });\n");
      out.write("\n");
      out.write("\n");
      out.write("        /**\n");
      out.write("         * 校验验证码\n");
      out.write("         * @param vcode\n");
      out.write("         */\n");
      out.write("        function checkVcode(vcode) {\n");
      out.write("            var res = false;\n");
      out.write("            $.ajax({\n");
      out.write("                type:\"post\",\n");
      out.write("                url:\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/sms/check\",\n");
      out.write("                data:{vcode: vcode},\n");
      out.write("                dataType:\"json\",\n");
      out.write("                async: false,\n");
      out.write("                success:function (data) {\n");
      out.write("                    if (data.code === 200){\n");
      out.write("                        res = true;\n");
      out.write("                    }\n");
      out.write("                }\n");
      out.write("            });\n");
      out.write("            return res;\n");
      out.write("        }\n");
      out.write("\n");
      out.write("        \n");
      out.write("    </script>\n");
      out.write("\n");
      out.write("    \n");
      out.write("    <!-- 底部结束 -->\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
